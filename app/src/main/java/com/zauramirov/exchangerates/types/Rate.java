package com.zauramirov.exchangerates.types;

import android.os.Parcel;
import android.os.Parcelable;

public class Rate implements Parcelable {
    public int r030;
    public String txt;
    public double rate;
    public String cc;
    public String exchangedate;

    public Rate(int r030, String txt, Double rate, String cc, String exchangedate){
        this.r030 = r030;
        this.txt = txt;
        this.rate = rate;
        this.cc = cc;
        this.exchangedate = exchangedate;
    }

    protected Rate(Parcel in) {
        r030 = in.readInt();
        txt = in.readString();
        rate = in.readDouble();
        cc = in.readString();
        exchangedate = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(r030);
        dest.writeString(txt);
        dest.writeDouble(rate);
        dest.writeString(cc);
        dest.writeString(exchangedate);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Rate> CREATOR = new Creator<Rate>() {
        @Override
        public Rate createFromParcel(Parcel in) {
            return new Rate(in);
        }

        @Override
        public Rate[] newArray(int size) {
            return new Rate[size];
        }
    };
}
