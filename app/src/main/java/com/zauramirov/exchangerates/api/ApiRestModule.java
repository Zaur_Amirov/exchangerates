package com.zauramirov.exchangerates.api;

import com.zauramirov.exchangerates.types.Rate;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiRestModule {
    @GET("exchange?json")
    Call<List<Rate>> rates();

}
