package com.zauramirov.exchangerates;

import com.zauramirov.exchangerates.controls.DBControl;
import com.zauramirov.exchangerates.controls.RatesControl;

public class Core {
    private static Core core = null;

    private RatesControl ratesControl = null;
    private DBControl dbControl = null;

    private Core() {
        ratesControl = new RatesControl();
        dbControl = new DBControl(App.getAppContext());
        dbControl.open();
    }

    public static Core get() {
        if (core == null) {
            core = new Core();
        }
        return core;
    }

    public RatesControl getRatesControl() {
        return ratesControl;
    }
    public DBControl getDBControl() {
        return dbControl;
    }
}
