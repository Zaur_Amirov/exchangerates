package com.zauramirov.exchangerates.controls;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.zauramirov.exchangerates.Utils;
import com.zauramirov.exchangerates.events.EventDataWasLoad;
import com.zauramirov.exchangerates.events.core.EventRouter;
import com.zauramirov.exchangerates.types.Rate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class DBControl {

    private final int DB_VERSION = 1;

    public static final String TABLE_CURRENCY = "currency";
    public static final String TABLE_EXCHANGE = "exchange";
    public static final String COLUMN_CURRENCY_ID = "currency_id";

    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_CODE = "code";

    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_EXCHANGE_DATE = "exchange_date";
    public static final String COLUMN_EXCHANGE_RATE = "exchange_rate";

    private final String LOG_TAG = "DB";
    private final String DB_NAME = "exchangerates.sqlite";

    private Context context;
    private DBHelper dbHelper;
    private SQLiteDatabase db;


    public DBControl(Context context) {
        this.context = context;
    }

    public void open() {
        dbHelper = new DBHelper(context, DB_NAME, null, DB_VERSION);
        db = dbHelper.getWritableDatabase();
    }

    public void close() {
        if (dbHelper != null)
            dbHelper.close();
    }

    public ArrayList<Map<String, Object>> selectFromTable(String query,
                                                          String[] keys) {
        Cursor c;
        c = db.rawQuery(query, null);
        ArrayList<Map<String, Object>> data = new ArrayList<>();
        HashMap<String, Object> m;
        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    m = new HashMap<>();
                    for (int i = 0; i < keys.length; i++)
                        m.put(keys[i],
                                (c.getString(c.getColumnIndex(keys[i])) != null ? c
                                        .getString(c.getColumnIndex(keys[i]))
                                        : ""));
                    data.add(m);

                } while (c.moveToNext());
            }
            c.close();
            return data;
        }
        return null;
    }


    public void setData(List<Rate> listOfCurrentRates) {
        EventRouter.send(new EventDataWasLoad());
        ArrayList<Map<String, Object>> dataExchange = selectFromTable("SELECT * FROM " + TABLE_EXCHANGE + " WHERE " + COLUMN_EXCHANGE_DATE + " = " + "\"" + listOfCurrentRates.get(0).exchangedate + "\";",
                new String[]{COLUMN_CURRENCY_ID, COLUMN_EXCHANGE_DATE, COLUMN_EXCHANGE_RATE});
        boolean insert = dataExchange.isEmpty();

        ArrayList<Map<String, Object>> dataCurrency;
        for (Rate rate : listOfCurrentRates) {

            dataCurrency = selectFromTable("SELECT * FROM " + TABLE_CURRENCY + " WHERE " + COLUMN_CURRENCY_ID + " = " + "\"" + rate.r030 + "\";",
                    new String[]{COLUMN_CURRENCY_ID, COLUMN_CODE, COLUMN_NAME});


            if (dataCurrency.isEmpty()) {
                final ContentValues cvCurrency = new ContentValues();
                cvCurrency.put(COLUMN_CURRENCY_ID, rate.r030);
                cvCurrency.put(COLUMN_CODE, rate.cc);
                cvCurrency.put(COLUMN_NAME, rate.txt);
                db.insert(TABLE_CURRENCY, null, cvCurrency);
                Utils.logD(LOG_TAG, "INSERT cvCurrency " + rate.cc );
            } else {
                Utils.logD(LOG_TAG, "UPDATE cvCurrency " + rate.cc );
                //TODO need update
            }

            final ContentValues cvExchange = new ContentValues();
            cvExchange.put(COLUMN_CURRENCY_ID, rate.r030);
            cvExchange.put(COLUMN_EXCHANGE_DATE, rate.exchangedate);
            cvExchange.put(COLUMN_EXCHANGE_RATE, rate.rate);
            if (insert) {
                db.insert(TABLE_EXCHANGE, null, cvExchange);
                Utils.logD(LOG_TAG, "INSERT " + rate.cc + " date = " + rate.exchangedate);
            } else {
                Utils.logD(LOG_TAG, "UPDATE " + rate.cc + " date = " + rate.exchangedate);
            }
        }

    }

    private class DBHelper extends SQLiteOpenHelper {

        public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
            super(context, name, factory, version);
            Utils.logD(LOG_TAG, "DBHelper constructor");
        }

        @Override
        public void onCreate(final SQLiteDatabase db) {
            db.execSQL("PRAGMA foreign_keys=on;");
            String sqlCreateTableCurrency = "CREATE TABLE " + TABLE_CURRENCY + " (" + COLUMN_CURRENCY_ID + " INTEGER PRIMARY KEY, " + COLUMN_NAME +
                    " TEXT, " + COLUMN_CODE + " TEXT);";

            Utils.logD(LOG_TAG, "command SQL >> " + sqlCreateTableCurrency);
            db.execSQL(sqlCreateTableCurrency);
            String sqlCreateTableExchange = "CREATE TABLE " + TABLE_EXCHANGE + " (" + COLUMN_ID + " INTEGER PRIMARY KEY, " + COLUMN_CURRENCY_ID + " INTEGER, "
                    + COLUMN_EXCHANGE_DATE + " TEXT, " + COLUMN_EXCHANGE_RATE + " REAL, " + "FOREIGN KEY (" + COLUMN_CURRENCY_ID + ") REFERENCES "
                    + TABLE_CURRENCY + "(" + COLUMN_CURRENCY_ID + "));";

            Utils.logD(LOG_TAG, "command SQL >> " + sqlCreateTableExchange);

            db.execSQL(sqlCreateTableExchange);

            Utils.logD(LOG_TAG, "CREATE table " + TABLE_EXCHANGE + " performed");
        }

        @Override
        public void onUpgrade(final SQLiteDatabase db, final int oldVersion, final int newVersion) {
            Utils.logD(LOG_TAG, "oldVersion = " + oldVersion + ", newVersion = " + newVersion);
        }
    }
}
