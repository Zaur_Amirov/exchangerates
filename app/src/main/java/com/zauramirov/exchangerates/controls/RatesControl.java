package com.zauramirov.exchangerates.controls;

import android.widget.Toast;

import com.zauramirov.exchangerates.App;
import com.zauramirov.exchangerates.Core;
import com.zauramirov.exchangerates.R;
import com.zauramirov.exchangerates.api.ApiRest;
import com.zauramirov.exchangerates.types.Rate;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RatesControl {

    private List<Rate> listOfCurrentRates = null;

    public void getRates() {
        if (App.isOnline()) {
            ApiRest.PointAccess().rates().enqueue(new Callback<List<Rate>>() {
                @Override
                public void onResponse(Call<List<Rate>> call, Response<List<Rate>> response) {

                    listOfCurrentRates = response.body();
                    if (listOfCurrentRates != null) {
                        Core.get().getDBControl().setData(listOfCurrentRates);
                    }
                }

                @Override
                public void onFailure(Call<List<Rate>> call, Throwable t) {

                }
            });
        } else {
            Toast.makeText(App.getAppContext(), R.string.no_internet_connection ,Toast.LENGTH_LONG).show();
        }
    }

    public List<Rate> getListOfCurrentRates() {
        return listOfCurrentRates;
    }
}
