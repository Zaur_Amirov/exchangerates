package com.zauramirov.exchangerates.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zauramirov.exchangerates.R;
import com.zauramirov.exchangerates.events.EventClickDetails;
import com.zauramirov.exchangerates.events.core.Event;
import com.zauramirov.exchangerates.events.core.EventListener;
import com.zauramirov.exchangerates.events.core.EventRouter;
import com.zauramirov.exchangerates.ui.MainActivity;

import androidx.fragment.app.Fragment;

public class DetailsCurrencyFragment extends Fragment implements EventListener {

    private TextView tvCode;
    private TextView tvName;
    private TextView tvSymbol;
    private TextView tvRate;
    private TextView tvDate;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_details, null);
        tvCode = rootView.findViewById(R.id.fragment_list_details_code);
        tvName = rootView.findViewById(R.id.fragment_list_details_name);
        tvSymbol = rootView.findViewById(R.id.fragment_list_details_symbol);
        tvRate = rootView.findViewById(R.id.fragment_list_details_rate);
        tvDate = rootView.findViewById(R.id.fragment_list_details_date);

        EventRouter.register(this);

        if (getArguments() != null && !getArguments().isEmpty()) {
            String tCode = getResources().getString(R.string.txt_code) + " : " + String.valueOf(getArguments().getInt(MainActivity.KEY_R30));
            tvCode.setText(tCode);
            String tName = getResources().getString(R.string.txt_name) + " : " + getArguments().getString(MainActivity.KEY_TXT);
            tvName.setText(tName);
            String tSymbol = getResources().getString(R.string.txt_symbol) + " : " + getArguments().getString(MainActivity.KEY_CC);
            tvSymbol.setText(tSymbol);
            String tRate = getResources().getString(R.string.txt_rate) + " : " + getArguments().getDouble(MainActivity.KEY_RATE);
            tvRate.setText(tRate);
            String tExchange = getResources().getString(R.string.txt_date) + " : " + getArguments().getString(MainActivity.KEY_EXCHANGEDATE);
            tvDate.setText(tExchange);
        }
        return rootView;
    }

    @Override
    public void onDestroy() {
        EventRouter.unregister(this);
        super.onDestroy();
    }

    @Override
    public void onEvent(Event event) {
        switch (event.getEventId()){
            case Event.EVENT_CLICK_DETAILS:
                EventClickDetails eventClickDetails = (EventClickDetails)event;
                String tCode = getResources().getString(R.string.txt_code) + " : " + eventClickDetails.getR030();
                tvCode.setText(tCode);
                String tName = getResources().getString(R.string.txt_name) + " : " + eventClickDetails.getTxt();
                tvName.setText(tName);
                String tSymbol = getResources().getString(R.string.txt_symbol) + " : " + eventClickDetails.getCc();
                tvSymbol.setText(tSymbol);
                String tRate = getResources().getString(R.string.txt_rate) + " : " + eventClickDetails.getRate();
                tvRate.setText(tRate);
                String tExchange = getResources().getString(R.string.txt_date) + " : " + eventClickDetails.getExchangedate();
                tvDate.setText(tExchange);
                break;
        }
    }

}
