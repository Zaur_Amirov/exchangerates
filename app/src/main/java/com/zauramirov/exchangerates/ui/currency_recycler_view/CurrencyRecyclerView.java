package com.zauramirov.exchangerates.ui.currency_recycler_view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zauramirov.exchangerates.R;
import com.zauramirov.exchangerates.ui.base_recycler_view.BaseRecyclerView;
import com.zauramirov.exchangerates.ui.base_recycler_view.BaseViewHolder;
import com.zauramirov.exchangerates.ui.base_recycler_view.RecyclerItems;

public class CurrencyRecyclerView extends BaseRecyclerView {
    public CurrencyRecyclerView(Context context) {
        super(context);
    }

    public CurrencyRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    @Override
    protected RecyclerItems createItems() {
        return new RecyclerItems();
    }

    @Override
    public BaseViewHolder create(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(getContext()).inflate(R.layout.item_currency, parent, false);
        return new CurrencyViewHolder(itemView);
    }
}
