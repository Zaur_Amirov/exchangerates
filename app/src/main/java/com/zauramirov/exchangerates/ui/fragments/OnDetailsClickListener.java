package com.zauramirov.exchangerates.ui.fragments;

public interface OnDetailsClickListener {
    void onDetailsClick(int r030, String txt, double rate, String cc, String exchangedate);
}
