package com.zauramirov.exchangerates.ui.currency_recycler_view;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;

import com.zauramirov.exchangerates.ui.base_recycler_view.BaseViewHolder;
import com.zauramirov.exchangerates.ui.base_recycler_view.RecyclerItem;
import com.zauramirov.exchangerates.ui.fragments.OnDetailsClickListener;


public class CurrencyRecyclerItem extends RecyclerItem implements OnClickListener {

    private int r030;
    private String txt;
    private double rate;
    private String cc;
    private String exchangedate;

    public CurrencyRecyclerItem(int r030, String txt, Double rate, String cc, String exchangedate) {
        this.r030 = r030;
        this.txt = txt;
        this.rate = rate;
        this.cc = cc;
        this.exchangedate = exchangedate;
    }

    @Override
    public void fill(BaseViewHolder holder) {
        if (holder instanceof CurrencyViewHolder) {
            ((CurrencyViewHolder) holder).init(this, txt);
        }
    }

    @Override
    public int getViewType() {
        return 0;
    }

    @Override
    public void onClick(View view) {
        Context context = view.getContext();
        if (context instanceof OnDetailsClickListener) {
            ((OnDetailsClickListener) context).onDetailsClick(r030, txt, rate, cc, exchangedate);
        }
    }
}
