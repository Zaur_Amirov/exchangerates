package com.zauramirov.exchangerates.ui.currency_recycler_view;

import android.view.View;
import android.widget.TextView;

import com.zauramirov.exchangerates.R;
import com.zauramirov.exchangerates.ui.base_recycler_view.BaseViewHolder;

public class CurrencyViewHolder extends BaseViewHolder {

    private View rootView;
    private TextView codeCurrency;

    public CurrencyViewHolder(View itemView) {
        super(itemView);
        codeCurrency = itemView.findViewById(R.id.item_currency_code);
        rootView = itemView.findViewById(R.id.item_of_list_currencies);
    }

    public void init(CurrencyRecyclerItem owner, String codeOfCurrency) {
        if (codeCurrency != null) {
            codeCurrency.setText(codeOfCurrency);
        }
        rootView.setOnClickListener(owner);
    }
}
