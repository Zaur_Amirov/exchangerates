package com.zauramirov.exchangerates.ui;

import android.os.Bundle;

import com.zauramirov.exchangerates.Core;
import com.zauramirov.exchangerates.R;
import com.zauramirov.exchangerates.events.EventClickDetails;
import com.zauramirov.exchangerates.events.core.EventRouter;
import com.zauramirov.exchangerates.ui.fragments.DetailsCurrencyFragment;
import com.zauramirov.exchangerates.ui.fragments.ListCurrenciesFragment;
import com.zauramirov.exchangerates.ui.fragments.OnDetailsClickListener;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

public class MainActivity extends AppCompatActivity implements OnDetailsClickListener {

    public static final String KEY_R30          = "code_currency";
    public static final String KEY_TXT          = "code_full_name";
    public static final String KEY_RATE         = "code_rate";
    public static final String KEY_CC           = "code_symbol";
    public static final String KEY_EXCHANGEDATE = "code_exchangedate";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Core.get().getRatesControl().getRates();
        if (findViewById(R.id.frgmCont) != null) {
            Fragment frag = new ListCurrenciesFragment();
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.frgmCont, frag);
            ft.commit();
        }
    }

    @Override
    public void onDetailsClick(int r030, String txt, double rate, String cc, String exchangedate) {
        if (findViewById(R.id.frgmCont) != null) {
            Fragment frag = new DetailsCurrencyFragment();
            Bundle bundle = new Bundle();

            bundle.putInt(KEY_R30, r030);
            bundle.putString(KEY_TXT, txt);
            bundle.putDouble(KEY_RATE, rate);
            bundle.putString(KEY_CC, cc);
            bundle.putString(KEY_EXCHANGEDATE, exchangedate);

            frag.setArguments(bundle);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.frgmCont, frag);
            ft.addToBackStack(null);
            ft.commit();
        } else {
            EventRouter.send(new EventClickDetails(r030, txt, rate, cc, exchangedate));
        }
    }
}
