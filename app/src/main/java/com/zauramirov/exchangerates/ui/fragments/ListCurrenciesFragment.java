package com.zauramirov.exchangerates.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zauramirov.exchangerates.Core;
import com.zauramirov.exchangerates.R;
import com.zauramirov.exchangerates.events.core.Event;
import com.zauramirov.exchangerates.events.core.EventListener;
import com.zauramirov.exchangerates.events.core.EventRouter;
import com.zauramirov.exchangerates.types.Rate;
import com.zauramirov.exchangerates.ui.base_recycler_view.RecyclerItems;
import com.zauramirov.exchangerates.ui.currency_recycler_view.CurrencyRecyclerItem;
import com.zauramirov.exchangerates.ui.currency_recycler_view.CurrencyRecyclerView;

import androidx.fragment.app.Fragment;

public class ListCurrenciesFragment extends Fragment implements EventListener {

    CurrencyRecyclerView currenciesList;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_list_currency, null);
        currenciesList = rootView.findViewById(R.id.list_currencies);
        currenciesList.init();
        RecyclerItems items = new RecyclerItems();
        if (Core.get().getRatesControl().getListOfCurrentRates() != null && !Core.get().getRatesControl().getListOfCurrentRates().isEmpty()) {
            for (Rate rate : Core.get().getRatesControl().getListOfCurrentRates()) {
                items.add(new CurrencyRecyclerItem(rate.r030, rate.txt, rate.rate, rate.cc, rate.exchangedate));
            }
        }
        currenciesList.animateTo(items);
        EventRouter.register(this);
        return rootView;
    }

    @Override
    public void onDestroy() {
        EventRouter.unregister(this);
        super.onDestroy();
    }

    @Override
    public void onEvent(Event event) {
        switch (event.getEventId()) {
            case Event.EVENT_DATA_WAS_LOAD:
                RecyclerItems items = new RecyclerItems();
                if (Core.get().getRatesControl().getListOfCurrentRates() != null && !Core.get().getRatesControl().getListOfCurrentRates().isEmpty()) {
                    for (Rate rate : Core.get().getRatesControl().getListOfCurrentRates()) {
                        items.add(new CurrencyRecyclerItem(rate.r030, rate.txt, rate.rate, rate.cc, rate.exchangedate));
                    }
                }
                currenciesList.animateTo(items);
                break;
        }
    }
}
