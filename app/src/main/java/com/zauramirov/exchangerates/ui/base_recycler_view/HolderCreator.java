package com.zauramirov.exchangerates.ui.base_recycler_view;

import android.view.ViewGroup;

public interface HolderCreator {
    BaseViewHolder create(ViewGroup parent, int viewType);
}
