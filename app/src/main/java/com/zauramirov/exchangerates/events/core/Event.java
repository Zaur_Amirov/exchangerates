package com.zauramirov.exchangerates.events.core;

public class Event {

    private int eventId;

    public static final int EVENT_DATA_WAS_LOAD = 100;

    public static final int EVENT_CLICK_DETAILS = 200;

    public Event(int eventId) {
        this.eventId = eventId;
    }

    public int getEventId() {
        return eventId;
    }

}
