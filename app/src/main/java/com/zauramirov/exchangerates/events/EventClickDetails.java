package com.zauramirov.exchangerates.events;

import com.zauramirov.exchangerates.events.core.Event;

public class EventClickDetails extends Event {

    private int r030;
    private String txt;
    private double rate;
    private String cc;
    private String exchangedate;
    public EventClickDetails(int r030, String txt, double rate, String cc, String exchangedate) {
        super(EVENT_CLICK_DETAILS);
        this.r030 = r030;
        this.txt = txt;
        this.rate = rate;
        this.cc = cc;
        this.exchangedate = exchangedate;
    }

    public double getRate() {
        return rate;
    }

    public int getR030() {
        return r030;
    }

    public String getTxt() {
        return txt;
    }

    public String getCc() {
        return cc;
    }

    public String getExchangedate() {
        return exchangedate;
    }
}
