package com.zauramirov.exchangerates.events.core;

import java.util.ArrayList;

public class EventRouter {
    private ArrayList<EventListener> listeners;
    protected static EventRouter router = new EventRouter();


    public EventRouter() {
        listeners = new ArrayList<>();
    }

    public static void register(EventListener listener) {
        router.register_(listener);
    }

    public static void unregister(EventListener listener) {
        router.unregister_(listener);
    }

    public static void send(Event event) {
        router.send_(event);
    }

    private void register_(EventListener listener) {
        if (listeners.indexOf(listener) == -1) {
            listeners.add(listener);
        }
    }

    private void unregister_(EventListener listener) {
        listeners.remove(listener);
    }

    private void send_(Event event) {
        for (EventListener listener : listeners) {
            listener.onEvent(event);
        }
    }
}
