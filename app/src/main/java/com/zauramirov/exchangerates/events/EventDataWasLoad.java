package com.zauramirov.exchangerates.events;

import com.zauramirov.exchangerates.events.core.Event;

public class EventDataWasLoad extends Event {
    public EventDataWasLoad() {
        super(EVENT_DATA_WAS_LOAD);
    }
}
