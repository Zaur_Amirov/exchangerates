package com.zauramirov.exchangerates.events.core;

public interface EventListener {
    void onEvent(Event event);
}
