package com.zauramirov.exchangerates;

import android.app.job.JobParameters;
import android.app.job.JobService;

public class PollService extends JobService {
    @Override
    public boolean onStartJob(JobParameters params) {
        Core.get().getRatesControl().getRates();
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return false;
    }
}
