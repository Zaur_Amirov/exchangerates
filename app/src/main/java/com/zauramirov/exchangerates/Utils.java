package com.zauramirov.exchangerates;

import android.util.Log;

public class Utils {
    private static final String TAG = "EXHANGE_RATES";

    /**
     * Utils.logD
     *
     * @param tag
     * @param msg
     */
    public static void logD(final String tag, final String msg) {
        if (BuildConfig.DEBUG) {
            Log.d(tag == null || tag.isEmpty() ? TAG : tag,
                    msg == null || msg.isEmpty() ? "Debug" : msg);
        }
    }

}
